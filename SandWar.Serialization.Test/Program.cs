﻿using System;
using System.Collections.Generic;
using System.IO;
using SandWar.Serialization.JSON;
using SandWar.Tools.Reflection;
using fastJSON;

namespace SandWar.Serialization.Test
{
	public sealed class Program
	{
		public class MyClass {
			public object SomeNull;
			public DateTime Value;
			public string SomeString;
			public object Another;
			public double Number;
			public double? NullableNumber;
			public int[] Numbers;
		}
		
		static void Main(string[] args) {
			var converter = new JSONConverter();
			
			var input = new MyClass() {
				Value = new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Utc),
				SomeString = "Hello, World!",
				Number = -2.34e-20,
				NullableNumber = 3,
				Numbers = new [] { 1, 2, 3 }
			};
			
			using (var tw = new StringWriter()) {
				converter.Serialize(input, tw);
				
				var serialized = tw.ToString();
				Console.WriteLine(serialized);
				
				using (var tr = new StringReader(serialized)) {
					var deserialized = converter.Deserialize<MyClass>(tr);
					Console.WriteLine(deserialized);
				}
			}
			
			//var json = JSON.ToJSON(input);
			
			//var output = new MyClass();
			//output = (MyClass)JSON.FillObject(output, json);
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}