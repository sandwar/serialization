﻿﻿using System;
using System.IO;
using System.Linq;

namespace SandWar.Serialization.JSON.Interfaces
{
    /// <summary>Provides a function to deserialize a type.</summary>
    public interface IDeserializer<T>
    {
        /// <summary>Read a value from a <see cref="TextReader" /> <paramref name="reader" />.</summary>
        T Deserialize(TextReader reader);
    }
}
