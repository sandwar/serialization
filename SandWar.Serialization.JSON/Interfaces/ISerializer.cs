﻿using System;
using System.IO;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>Provides a function to serialize a <see cref="Type" />.</summary>
	public interface ISerializer<T>
	{
		/// <summary>Write a <paramref name="value" /> to a <see cref="TextWriter" /> <paramref name="writer" />.</summary>
		void Serialize(T value, TextWriter writer);
	}
}
