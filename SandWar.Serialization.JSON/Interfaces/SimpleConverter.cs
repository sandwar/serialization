﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using SandWar.Tools.Reflection;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>Simplifies writing coverters at the cost of an extra downcast and a function call.</summary>
	public abstract class SimpleConverter<T> : ITypeConverter, IConverter<T>
	{
		readonly Type[] types = { typeof(T) };
		public IEnumerable<Type> SupportedTypes {
			get { return types; }
		}
		
		public abstract void Serialize(T value, TextWriter writer);
		
		public abstract T Deserialize(TextReader reader);
		
		public Expression BuildDeserializer(ITypeConvertersCollection generator, Type type) {
			var reader = Expression.Parameter(typeof(TextReader));
			
			return Expression.Lambda(
				Expression.GetFuncType(typeof(TextReader), type),
				Expression.Call(
					Expression.Constant(this, typeof(SimpleConverter<T>)),
					Finder.GetMethod<SimpleConverter<T>>(converter => converter.Deserialize(default(TextReader))),
					reader
				),
				new [] { reader }
			);
		}
		
		public Expression BuildSerializer(ITypeConvertersCollection generator, Type type) {
			var value = Expression.Parameter(type);
			var writer = Expression.Parameter(typeof(TextWriter));
			
			return Expression.Lambda(
				Expression.GetActionType(type, typeof(TextWriter)),
				Expression.Call(
					Expression.Constant(this, typeof(SimpleConverter<T>)),
					Finder.GetMethod<SimpleConverter<T>>(converter => converter.Serialize(default(T), default(TextWriter))),
					Expression.Convert(value, type),
					writer
				),
				new [] { value, writer }
			);
		}
	}
}
