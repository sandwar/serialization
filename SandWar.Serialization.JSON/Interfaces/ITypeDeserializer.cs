﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>Provides tools to build deserializers for some <see cref="Type" />s.</summary>
	public interface ITypeDeserializer
	{
		/// <summary>The list of supported <see cref="Type" />s.</summary>
		IEnumerable<Type> SupportedTypes { get; }
		
		/// <summary>Build an expression to read a value of <see cref="Type" /> <paramref name="type" /> from a <see cref="System.IO.TextReader" />.</summary>
		/// <returns><c>Expression&lt;Func&lt;TextReader, <paramref name="type" />&gt;&gt;</c></returns>
		Expression BuildDeserializer(ITypeConvertersCollection generator, Type type);
	}
}
