﻿using System;
using System.Linq.Expressions;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>A collection of fast <see cref="ITypeSerializer" />s and <see cref="ITypeDeserializer" />s.</summary>
	public interface ITypeConvertersCollection
	{
		/// <summary>Register a serializer to be used for base types.</summary>
		void RegisterSerializer(ITypeSerializer serializer);
		
		/// <summary>Register a deserializer to be used for base types.</summary>
		void RegisterDeserializer(ITypeDeserializer deserializer);
		
		/// <summary>Get a serializer expression for a <see cref="Type" /> or any of it's base <see cref="Type" />s.</summary>
		/// <returns><c>Expression&lt;Action&lt;<paramref name="type" />, TextWriter&gt;&gt;</c></returns>
		bool TryGetSerializerExpression(Type type, out Expression expression);
		
		/// <summary>Get a serializer expression for a <see cref="Type" /> or any of it's base <see cref="Type" />s.</summary>
		/// <returns><c>Expression&lt;Func&lt;TextReader, <paramref name="type" />&gt;&gt;</c></returns>
		bool TryGetDeserializerExpression(Type type, out Expression expression);
	}
}
