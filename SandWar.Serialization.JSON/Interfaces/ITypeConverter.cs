﻿using System;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>Provides tools build serializers and deserializers for some <see cref="Type" />s.</summary>
	public interface ITypeConverter : ITypeSerializer, ITypeDeserializer
	{
		
	}
}
