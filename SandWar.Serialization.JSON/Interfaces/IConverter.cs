﻿﻿using System;
using SandWar.Serialization.JSON.Interfaces;

namespace SandWar.Serialization.JSON
{
    /// <summary>Provides functions to serialize and deserialize a <see cref="Type" />.</summary>
    public interface IConverter<T> : ISerializer<T>, IDeserializer<T>
    {
        
    }
}
