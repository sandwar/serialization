﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SandWar.Serialization.JSON.Interfaces
{
	/// <summary>Provides tools to build serializers for some <see cref="Type" />s.</summary>
	public interface ITypeSerializer
	{
		/// <summary>The list of supported <see cref="Type" />s.</summary>
		IEnumerable<Type> SupportedTypes { get; }
		
		/// <summary>Build an expression to write a value of <see cref="Type" /> <paramref name="type" /> to a <see cref="System.IO.TextWriter" />.</summary>
		/// <returns><c>Expression&lt;Action&lt;<paramref name="type" />, TextWriter&gt;&gt;</c></returns>
		Expression BuildSerializer(ITypeConvertersCollection generator, Type type);
	}
}
