﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using SandWar.Serialization.JSON.Converters;
using System.Linq;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON
{
	/// <summary>Provides tools to serialize and deserialize JSON streams.</summary>
	public class JSONConverter
	{
		readonly TypeConvertersCollection converters;
		
		readonly Dictionary<Type, object> compiledConverters = new Dictionary<Type, object>();
		
		public JSONConverter() {
			converters = new TypeConvertersCollection();
			
			var stringConverter = new StringConverter();
			converters.RegisterSerializer(stringConverter);
			converters.RegisterDeserializer(stringConverter);
			
			var doubleConverter = new DoubleConverter();
			converters.RegisterSerializer(doubleConverter);
			converters.RegisterDeserializer(doubleConverter);
			
			var intConverter = new IntConverter();
			converters.RegisterSerializer(intConverter);
			converters.RegisterDeserializer(intConverter);
			
			var dateTimeConverter = new DateTimeConverter();
			converters.RegisterSerializer(dateTimeConverter);
			converters.RegisterDeserializer(dateTimeConverter);
			
			var objectConverter = new ObjectConverter();
			converters.RegisterSerializer(objectConverter);
			converters.RegisterDeserializer(objectConverter);
			
			var nullableConverter = new NullableConverter();
			converters.RegisterSerializer(nullableConverter);
			converters.RegisterDeserializer(nullableConverter);
			
			var enumerableSerializer = new EnumerableSerializer();
			converters.RegisterSerializer(enumerableSerializer);
			
			var arrayDeserializer = new ArrayDeserializer();
			converters.RegisterDeserializer(arrayDeserializer);
		}
		
		/// <summary>Deserialize an object read from a JSON strem.</summary>
		public T Deserialize<T>(TextReader reader) {
			return GetConverter<T>().Deserialize(reader);
		}
		
		/// <summary>Serialize an object and write it to a JSON stream.</summary>
		public void Serialize<T>(T value, TextWriter writer) {
			GetConverter<T>().Serialize(value, writer);
		}
		
		/// <summary>Build a converter to serialize a specific type quickly.</summary>
		public IConverter<T> GetConverter<T>() {
			var type = typeof(T);
			
			// Do we already have a compiled serializer for this type?
			object compiledConverter;
			if (compiledConverters.TryGetValue(type, out compiledConverter))
				return (Converter<T>)compiledConverter;
			
			// Do we have an uncompiled serializer expression for this type or it's base types?
			Expression serializerExpression;
			if (!converters.TryGetSerializerExpression(type, out serializerExpression))
				throw Errors.NoSerializer(type);
			
			// Same as above, for deserializer expression...
			Expression deserializerExpression;
			if (!converters.TryGetDeserializerExpression(type, out deserializerExpression))
				throw Errors.NoDeserializer(type);
			
			var genericSerializer = new Converter<T>((Expression<Action<T, TextWriter>>)serializerExpression, (Expression<Func<TextReader, T>>)deserializerExpression);
			compiledConverters.Add(type, genericSerializer);
			return genericSerializer;
		}
	}
}
