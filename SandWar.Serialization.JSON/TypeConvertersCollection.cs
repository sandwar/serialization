﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SandWar.Serialization.JSON.Interfaces;

namespace SandWar.Serialization.JSON
{
	/// <summary>Provides tools to generate a fast serializers/deserializers.</summary>
	public class TypeConvertersCollection : ITypeConvertersCollection
	{
		readonly Dictionary<Type, ITypeSerializer> serializers = new Dictionary<Type, ITypeSerializer>();
		readonly Dictionary<Type, ITypeDeserializer> deserializers = new Dictionary<Type, ITypeDeserializer>();
		
		public void RegisterSerializer(ITypeSerializer serializer) {
			foreach (var type in serializer.SupportedTypes) {
				try {
					serializers.Add(type, serializer);
				} catch(ArgumentException) {
					throw new Exception(String.Format("Another serializer for the type {0} has already been registered.", type.FullName));
				}
			}
		}
		
		public void RegisterDeserializer(ITypeDeserializer deserializer) {
			foreach (var type in deserializer.SupportedTypes) {
				try {
					deserializers.Add(type, deserializer);
				} catch(ArgumentException) {
					throw new Exception(String.Format("Another deserializer for the type {0} has already been registered.", type.FullName));
				}
			}
		}
		
		public bool TryGetSerializerExpression(Type type, out Expression expression) {
			// Do we have a serializer for this exact type or a base type?
			ITypeSerializer serializer;
			var baseType = type;
			do {
				try {
					if (serializers.TryGetValue(baseType, out serializer)) {
						expression = serializer.BuildSerializer(this, type);
						return true;
					}
					
					if (baseType.IsGenericType) {
						if (serializers.TryGetValue(baseType.GetGenericTypeDefinition(), out serializer)) {
							expression = serializer.BuildSerializer(this, type);
							return true;
						}
					}
					
					foreach (var interf in baseType.GetInterfaces()) {
						if (serializers.TryGetValue(interf, out serializer)) {
							expression = serializer.BuildSerializer(this, type);
							return true;
						}
						
						if (interf.IsGenericType) {
							if (serializers.TryGetValue(interf.GetGenericTypeDefinition(), out serializer)) {
								expression = serializer.BuildSerializer(this, type);
								return true;
							}
						}
					}
				} catch(NotImplementedException) {
					// Some serializers might throw NotImplementedExceptions, we won't use them.
				}
			} while((baseType = baseType.BaseType) != null);
			
			expression = null;
			return false;
		}
		
		public bool TryGetDeserializerExpression(Type type, out Expression expression) {
			// Do we have a deserializer for this exact type or a base type?
			ITypeDeserializer deserializer;
			var baseType = type;
			do {
				try {
					if (deserializers.TryGetValue(baseType, out deserializer)) {
						expression = deserializer.BuildDeserializer(this, type);
						return true;
					}
					
					if (baseType.IsGenericType) {
						if (deserializers.TryGetValue(baseType.GetGenericTypeDefinition(), out deserializer)) {
							expression = deserializer.BuildDeserializer(this, type);
							return true;
						}
					}
					
					foreach (var interf in baseType.GetInterfaces()) {
						if (deserializers.TryGetValue(interf, out deserializer)) {
							expression = deserializer.BuildDeserializer(this, type);
							return true;
						}
						
						if (interf.IsGenericType) {
							if (deserializers.TryGetValue(interf.GetGenericTypeDefinition(), out deserializer)) {
								expression = deserializer.BuildDeserializer(this, type);
								return true;
							}
						}
					}
				} catch(NotImplementedException) {
					// Some serializers might throw NotImplementedExceptions, we won't use them.
				}
			} while((baseType = baseType.BaseType) != null);
			
			expression = null;
			return false;
		}
	}
}