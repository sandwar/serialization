﻿using System;
using System.IO;
using System.Linq.Expressions;

namespace SandWar.Serialization.JSON.Internal
{
	class Converter<T> : IConverter<T>
	{
		readonly Action<T, TextWriter> serializer;
		readonly Func<TextReader, T> deserializer;
		
		public Converter(Expression<Action<T, TextWriter>> serializer, Expression<Func<TextReader, T>> deserializer) {
			this.serializer = serializer == null ? serializeFail : serializer.Compile();
			this.deserializer = deserializer == null ? deserializeFail : deserializer.Compile();
		}
		
		public void Serialize(T value, TextWriter writer) {
			serializer(value, writer);
		}

		public T Deserialize(TextReader reader) {
			return deserializer(reader);
		}
		
		static void serializeFail(T value, TextWriter writer) {
			throw Errors.NoSerializer(typeof(T));
		}
		
		static T deserializeFail(TextReader writer) {
			throw Errors.NoDeserializer(typeof(T));
		}
	}
}
