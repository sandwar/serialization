﻿using System;
using System.IO;
using System.Linq.Expressions;
using System.Text;

namespace SandWar.Serialization.JSON.Internal
{
	static class JSONTools
	{
		// http://json.org/
		
		#region Parsing
		
		/// <summary>Skip all whitespaces and stop at first non-whitespace character.</summary>
		public static void SkipSpaces(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipSpaces");
			
			while (true) {
				var read = reader.Peek();
				if (read == -1 || !Char.IsWhiteSpace(unchecked((char)read)))
					return;
				reader.Read();
			}
		}
		
		/// <summary>
		/// Read a string, assumes no leading whitespaces.
		/// 
		/// Format: <c>"abc\\def\nghi\u1234"</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		public static bool ReadString(TextReader reader, out string value) {
			//System.Diagnostics.Debug.WriteLine("ReadString");
			
			bool isNull;
			if (!ReadNull(reader, out isNull)) {
				value = null;
				return false;
			}
			
			if (isNull) {
				value = null;
				return true;
			}
			
			var read = reader.Read();
			if (read != '"') {
				value = null;
				return false;
			}
			
			var sb = new StringBuilder();
			while (true) {
				read = reader.Read();
				char readChar = unchecked((char)read);
				if (readChar == '"') {
					value = sb.ToString();
					return true;
				}
				
				if (read == -1 || Char.IsControl(readChar)) {
					value = null;
					return false;
				}
				
				if (readChar == '\\') {
					switch(reader.Read()) {
					case '"':
						sb.Append('"');
						break;
					case '\\':
						sb.Append('\\');
						break;
					case '/':
						sb.Append('/');
						break;
					case 'b':
						sb.Append('\b');
						break;
					case 'f':
						sb.Append('\f');
						break;
					case 'n':
						sb.Append('\n');
						break;
					case 'r':
						sb.Append('\r');
						break;
					case 't':
						sb.Append('\t');
						break;
					case 'u':
						if (!parseUnicodeHexSequence(reader, sb)) {
							value = null;
							return false;
						}
						break;
					//case -1:
					default:
						value = null;
						return false;
					}
				}
				
				sb.Append(readChar);
			}
		}
		
		/// <summary>
		/// Read a number, assumes no leading whitespaces.
		/// 
		/// Format: <c>-123.456e+102</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		public static bool ReadNumber(TextReader reader, out double value) {
			//System.Diagnostics.Debug.WriteLine("ReadNumber");
			
			double sign = +1;
			double integralPart = 0;
			double decimalPart = 0;
			double exponent = 0;
			
			// Process sign (if any)
			var read = reader.Read();  // read first digit or '-' sign
			if (read == '-') {
				sign = -1;
				read = reader.Read();  // read first digit
			}
			
			// We already have an unprocessed char in 'read' when we get here
			
			// Process integral part
			if (read == '0') {
				// nothing to do with integral part if we have a zero
			} else if (read >= '1' && read <= '9') {
				integralPart = read - '0';
				
				while (true) {
					read = reader.Peek();
					if (read < '0' || read > '9')
						break;
					
					integralPart = integralPart * 10 + read - '0';
					read = reader.Read();  // skip this digit
				}
			} else {
				value = 0;
				return false;
			}
			
			// Process decimal part
			read = reader.Peek();
			if (read == '.') {
				read = reader.Read();  // skip the '.' sign
				
				double decimalValue = 1;
				while (true) {
					read = reader.Peek();
					if (read < '0' || read > '9')
						break;
					
					decimalValue *= 0.1;
					decimalPart += (read - '0') * decimalValue;
					read = reader.Read();  // skip this digit
				}
			}
			
			// Process the exponential part
			read = reader.Peek();
			if (read == 'e' || read == 'E') {
				double exponentSign = +1;
				exponent = 0;
				
				read = reader.Read();  // skip the E
				read = reader.Peek();
				if (read == '-') {
					exponentSign = -1;
					read = reader.Read();  // skip the '-' sign
				} else if (read == '+') {
					//exponentSign = +1;
					read = reader.Read();  // skip the '+' sign
				}
				
				while (true) {
					read = reader.Peek();
					if (read < '0' || read > '9')
						break;
					
					exponent = exponent * 10 + read - '0';
					read = reader.Read();  // skip this digit
				}
				
				exponent *= exponentSign;
			}
			
			// This function arrives here as soon as it finds a non-numerical character;
			// it should report success only if next char is ',', '}', ']' or a space.
			if (read == ',' || read == '}' || read == ']' || Char.IsWhiteSpace(unchecked((char)read))) {
				value = sign * (integralPart + decimalPart) * Math.Pow(10, exponent);
				return true;
			}
			
			value = 0;
			return false;
		}
		
		/// <summary>
		/// Read a boolean, assumes no leading whitespaces.
		/// 
		/// Format: <c>true</c> or <c>false</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		public static bool ReadBoolean(TextReader reader, out bool value) {
			//System.Diagnostics.Debug.WriteLine("ReadBoolean");
			
			var read = reader.Read();
			if (read == 't' && reader.Read() == 'r' && reader.Read() == 'u' && reader.Read() == 'e') {
				value = true;
				return true;
			}
			
			if (read == 'f' && reader.Read() == 'a' && reader.Read() == 'l' && reader.Read() == 's' && reader.Read() == 'e') {
				value = false;
				return true;
			}
			
			value = false;
			return false;
		}
		
		/// <summary>
		/// Read a null value, assumes no leading whitespaces.
		/// 
		/// Format: <c>null</c>.
		/// 
		/// Spoils the stream if it fails but never fails with valid JSON.
		/// </summary>
		public static bool ReadNull(TextReader reader, out bool isNull) {
			//System.Diagnostics.Debug.WriteLine("ReadNull");
			
			var read = reader.Peek();
			if (read != 'n') {
				isNull = false;
				return true;
			}
			
			reader.Read();
			if (reader.Read() != 'u' || reader.Read() != 'l' || reader.Read() != 'l') {
				isNull = false;
				return false;
			}
			
			isNull = true;
			return true;
		}
		
		/// <summary>
		/// Skips the start of an object (if any), assumes no leading whitespaces.
		/// 
		/// Format: <c>{</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an object start was skipped.</returns>
		public static bool SkipObjectStart(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipObjectStart");
			
			//var read = reader.Peek();
			//if (read != '{')
			//	return false;
			//
			//reader.Read();
			//return true;
			
			var read = reader.Read();
			return read == '{';
		}
		
		/// <summary>
		/// Skips an object separator (if any), assumes no leading whitespaces.
		/// 
		/// Format: <c>,</c>.
		/// 
		/// Doesn't spoil the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an object start was skipped.</returns>
		public static bool SkipFieldSeparator(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipObjectSeparator");
			
			var read = reader.Peek();
			if (read != ',')
				return false;
			
			reader.Read();
			return true;
			
			//var read = reader.Read();
			//return read == ',';
		}
		
		/// <summary>
		/// Skips an object field separator (if any), assumes no leading whitespaces.
		/// 
		/// Format: <c>:</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an object field start was skipped.</returns>
		public static bool SkipObjectFieldSeparator(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipObjectFieldSeparator");
			
			//var read = reader.Peek();
			//if (read != ':')
			//	return false;
			//
			//reader.Read();
			//return true;
			
			var read = reader.Read();
			return read == ':';
		}
		
		/// <summary>
		/// Skips the end of an object (if any), assumes no leading whitespaces.
		/// 
		/// Format: <c>}</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an object start was skipped.</returns>
		public static bool SkipObjectEnd(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipObjectEnd");
			
			//var read = reader.Peek();
			//if (read != '}')
			//	return false;
			//
			//reader.Read();
			//return true;
			
			var read = reader.Read();
			return read == '}';
		}
		
		/// <summary>
		/// Check if next token is an object end, assumes no leading whitespaces.
		/// 
		/// Format: <c>}</c>.
		/// 
		/// Doesn't spoil the stream.
		/// </summary>
		public static bool IsObjectEnd(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("IsObjectEnd");
			
			return reader.Peek() == '}';
		}
		
		/// <summary>
		/// Check if next token is an array end, assumes no leading whitespaces.
		/// 
		/// Format: <c>]</c>.
		/// 
		/// Doesn't spoil the stream.
		/// </summary>
		public static bool IsArrayEnd(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("IsArrayEnd");
			
			return reader.Peek() == ']';
		}
		
		/// <summary>
		/// Skips the start of an array (any), assumes no leading whitespaces.
		/// 
		/// Format: <c>[</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an array start was skipped.</returns>
		public static bool SkipArrayStart(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipArrayStart");
			
			//var read = reader.Peek();
			//if (read != '[')
			//	return false;
			//
			//reader.Read();
			//return true;
			
			var read = reader.Read();
			return read == '[';
		}
		
		/// <summary>
		/// Skips the end of an array (if any), assumes no leading whitespaces.
		/// 
		/// Format: <c>]</c>.
		/// 
		/// Spoils the stream if it fails.
		/// </summary>
		/// <returns>A boolean value indicating if an array start was skipped.</returns>
		public static bool SkipArrayEnd(TextReader reader) {
			//System.Diagnostics.Debug.WriteLine("SkipArrayEnd");
			
			//var read = reader.Peek();
			//if (read != ']')
			//	return false;
			//
			//reader.Read();
			//return true;
			
			var read = reader.Read();
			return read == ']';
		}
		
		static bool parseUnicodeHexSequence(TextReader reader, StringBuilder sb) {
			int total = 0;
			for (var shift = 0; shift != 16; shift += 2) {
				var read = reader.Read();
				int parsed;
				if (read >= '0' && read <= '9')
					parsed = read - '0';
				else if (read >= 'a' && read <= 'z')
					parsed = read - 'a';
				else if (read >= 'A' && read <= 'Z')
					parsed = read - 'A';
				else
					return false;
				total |= parsed << shift;
			}
			sb.Append(unchecked((char)total));
			return true;
		}
		
		#endregion
		
		#region Generating
		
		/// <summary>Write an escaped string, <c>null</c> is allowed.</summary>
		/// <param name="writer"></param>
		/// <param name="str"></param>
		public static void WriteString(TextWriter writer, string str) {
			//System.Diagnostics.Debug.WriteLine("WriteString");
			
			if (str == null) {
				WriteNull(writer);
				return;
			}
			
			writer.Write('"');
			
			foreach (var chr in str) {
				switch (chr) {
				case '"':
					writer.Write("\\\"");
					break;
				case '\\':
					writer.Write("\\\\");
					break;
				case '\n':  // not required for JSON-compliant deserializers, but widens compatibility
					writer.Write("\\n");
					break;
				case '\r':  // not required for JSON-compliant deserializers, but widens compatibility
					writer.Write("\\r");
					break;
				default:
					writer.Write(chr);
					break;
				}
			}
			
			writer.Write('"');
		}
		
		/// <summary>Write an escaped number.</summary>
		public static void WriteNumber(TextWriter writer, double number) {
			//System.Diagnostics.Debug.WriteLine("WriteNumber");
			
			// TODO: verify that the format is actually equivalent and there is no loss in precision
			writer.Write(number.ToString("G"));
		}
		
		/// <summary>Write a boolean value.</summary>
		public static void WriteBoolean(TextWriter writer, bool value) {
			//System.Diagnostics.Debug.WriteLine("WriteBoolean");
			
			writer.Write(value ? "true" : "false");
		}
		
		/// <summary>Write a null value.</summary>
		public static void WriteNull(TextWriter writer) {
			//System.Diagnostics.Debug.WriteLine("WriteNull");
			
			writer.Write("null");
		}
		
		#endregion
		
		#region Utilities
		
		/// <summary>Throw a <see cref="FormatException" />.</summary>
		public static void Fail(string message, params object[] args) {
			//System.Diagnostics.Debug.WriteLine("Fail");
			
			throw new FormatException(String.Format(message, args));
		}
		
		#endregion
	}
}
