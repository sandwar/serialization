﻿using System;

namespace SandWar.Serialization.JSON.Internal
{
	static class Errors
	{
		public static NotSupportedException NoSerializer(Type type) {
			return new NotSupportedException(String.Format("No serializer for type {0} found.", type.FullName));
		}
		
		public static NotSupportedException NoDeserializer(Type type) {
			return new NotSupportedException(String.Format("No deserializer for type {0} found.", type.FullName));
		}
	}
}
