﻿using System;
using System.Globalization;
using System.IO;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize <see cref="DateTime" />s.</summary>
	public class DateTimeConverter : SimpleConverter<DateTime>
	{
		public override void Serialize(DateTime value, TextWriter writer) {
			writer.Write(value.ToUniversalTime().ToString("'\"'yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z\"'"));
		}
		
		public override DateTime Deserialize(TextReader reader) {
			string date;
			if (!JSONTools.ReadString(reader, out date))
				throw new InvalidDataException();  // failed to parse
			
			return DateTime.ParseExact(date, "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal).ToUniversalTime();
		}
	}
}
