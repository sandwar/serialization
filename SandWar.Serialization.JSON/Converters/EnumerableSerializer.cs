﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using SandWar.Tools.Reflection;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	public class EnumerableSerializer : ITypeSerializer
	{
		readonly Type[] types = { typeof(IEnumerable<>) };
		public IEnumerable<Type> SupportedTypes {
			get { return types; }
		}
		
		public Expression BuildSerializer(ITypeConvertersCollection generator, Type type) {
			var innerType = type.GetInterface(typeof(IEnumerable<>)).GetGenericArguments()[0];
			
			Expression innerSerializer;
			if (!generator.TryGetSerializerExpression(innerType, out innerSerializer))
				throw Errors.NoSerializer(innerType);
			
			var jsonWriteNull = Finder.GetMethod(() => JSONTools.WriteNull(default(TextWriter)));
			//var textWriterWriteString = Finder.GetMethod<TextWriter>(w => w.Write(default(string)));
			var textWriterWriteChar = Finder.GetMethod<TextWriter>(w => w.Write(default(char)));
			var getEnumerator = Finder.GetMethod<IEnumerable<object>>(enumerable => enumerable.GetEnumerator()).GetMethodDefinitionForType(typeof(IEnumerable<>).MakeGenericType(innerType));
			var moveNext = Finder.GetMethod<IEnumerator>(enumer => enumer.MoveNext());
			var current = Finder.GetProperty<IEnumerator<object>>(enumer => enumer.Current).GetPropertyDefinitionForType(getEnumerator.ReturnType);
			
			var value = Expression.Parameter(type);
			var writer = Expression.Parameter(typeof(TextWriter));
			
			var enumerator = Expression.Parameter(getEnumerator.ReturnType);
			var isNotFirst = Expression.Parameter(typeof(bool));
			var breakLabel = Expression.Label();
			
			return Expression.Lambda(
				Expression.GetActionType(type, typeof(TextWriter)),
				Expression.IfThenElse(
					Expression.Equal(value, Expression.Constant(null, type)),
					Expression.Call(jsonWriteNull, writer),
					Expression.Block(
						new [] { enumerator, isNotFirst },
						Expression.Call(writer, textWriterWriteChar, Expression.Constant('[')),
						Expression.Assign(enumerator, Expression.Call(value, getEnumerator)),
						//Expression.Assign(isNotFirst, Expression.Constant(false)),  // implicit?
						Expression.Loop(
							Expression.IfThenElse(
								Expression.Call(enumerator, moveNext),
								Expression.Block(
									Expression.IfThen(
										isNotFirst,
										Expression.Call(writer, textWriterWriteChar, Expression.Constant(','))
									),
									Expression.Invoke(innerSerializer, Expression.Property(enumerator, current), writer),
									Expression.Assign(isNotFirst, Expression.Constant(true))
								),
								Expression.Break(breakLabel)
							),
							breakLabel
						),
						Expression.Call(writer, textWriterWriteChar, Expression.Constant(']'))
					)
				),
				value, writer
			);
		}
	}
}
