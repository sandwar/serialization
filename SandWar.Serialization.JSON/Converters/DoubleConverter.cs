﻿using System;
using System.IO;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize <see cref="Double" />s.</summary>
	public class DoubleConverter : SimpleConverter<double>
	{
		public override void Serialize(double value, TextWriter writer) {
			JSONTools.WriteNumber(writer, value);
		}
		
		public override double Deserialize(TextReader reader) {
			double value;
			if (!JSONTools.ReadNumber(reader, out value))
				throw new InvalidDataException();  // failed to parse
			
			return value;
		}
	}
}
