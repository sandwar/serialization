﻿using System;
using System.IO;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize <see cref="String" />s.</summary>
	public class StringConverter : SimpleConverter<string>
	{
		public override void Serialize(string value, TextWriter writer) {
			JSONTools.WriteString(writer, value);
		}
		
		public override string Deserialize(TextReader reader) {
			string value;
			if (!JSONTools.ReadString(reader, out value))
				throw new InvalidDataException();  // failed to parse
			
			return value;
		}
	}
}
