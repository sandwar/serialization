﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using SandWar.Tools.Reflection;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize <see cref="Nullable" />s.</summary>
	public class NullableConverter : ITypeConverter
	{
		readonly Type[] types = { typeof(Nullable<>) };
		public IEnumerable<Type> SupportedTypes {
			get { return types; }
		}
		
		public Expression BuildSerializer(ITypeConvertersCollection generator, Type type) {
			var innerType = Nullable.GetUnderlyingType(type);
			
			Expression innerSerializer;
			if (!generator.TryGetSerializerExpression(innerType, out innerSerializer))
				throw Errors.NoSerializer(innerType);
			
			var jsonWriteNull = Finder.GetMethod(() => JSONTools.WriteNull(default(TextWriter)));
			var nullableHasValue = Finder.GetProperty<int?>(nullable => nullable.HasValue).GetPropertyDefinitionForType(type);
			var nullableValue = Finder.GetProperty<int?>(nullable => nullable.Value).GetPropertyDefinitionForType(type);
			
			var value = Expression.Parameter(type);
			var writer = Expression.Parameter(typeof(TextWriter));
			
			return Expression.Lambda(
				Expression.IfThenElse(
					Expression.Property(value, nullableHasValue),
					Expression.Invoke(innerSerializer, Expression.Property(value, nullableValue), writer),
					Expression.Call(jsonWriteNull, writer)
				),
				value,
				writer
			);
		}
		
		public Expression BuildDeserializer(ITypeConvertersCollection generator, Type type) {
			var innerType = Nullable.GetUnderlyingType(type);
			
			Expression innerDeserializer;
			if (!generator.TryGetDeserializerExpression(innerType, out innerDeserializer))
				throw Errors.NoDeserializer(innerType);
			
			var readNull = Finder.GetMethod(() => JSONTools.ReadNull(default(TextReader), out Finder.Type<bool>.V));
			var fail = Finder.GetMethod(() => JSONTools.Fail(default(string)));
			
			var reader = Expression.Parameter(typeof(TextReader));
			
			var isValueNull = Expression.Parameter(typeof(bool));
			
			return Expression.Lambda(
				Expression.GetFuncType(typeof(TextReader), type),
				Expression.Block(
					new [] { isValueNull },
					Expression.IfThen(
						Expression.IsFalse(Expression.Call(readNull, reader, isValueNull)),
						Expression.Call(fail, Expression.Constant("Invalid token."), Expression.Constant(new object[0]))
					),
					Expression.Condition(
						isValueNull,
						Expression.Constant(null, type),
						Expression.Convert(Expression.Invoke(innerDeserializer, reader), type)
					)
				),
				reader
			);
		}
	}
}
