﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using SandWar.Tools.Reflection;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize any non-native type (to be used as fallback).</summary>
	public class ObjectConverter : ITypeConverter
	{
		readonly Type[] types = { typeof(object) };
		public IEnumerable<Type> SupportedTypes {
			get { return types; }
		}
		
		public Expression BuildSerializer(ITypeConvertersCollection generator, Type type) {
			// FIXME: StackOverflow with recursive types
			
			var value = Expression.Parameter(type);
			var writer = Expression.Parameter(typeof(TextWriter));
			
			var textWriterWriteString = Finder.GetMethod<TextWriter>(w => w.Write(default(string)));
			var textWriterWriteChar = Finder.GetMethod<TextWriter>(w => w.Write(default(char)));
			
			var jsonWriteNull = Finder.GetMethod(() => JSONTools.WriteNull(default(TextWriter)));
			
			var expressions = new List<Expression>();
			
			var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => !field.HasAttribute<NonSerializedAttribute>());
			var isFirst = true;
			
			expressions.Add(Expression.Call(writer, textWriterWriteChar, Expression.Constant('{')));
			foreach (var field in fields) {
				Expression serializer;
				if (!generator.TryGetSerializerExpression(field.FieldType, out serializer))
					throw Errors.NoSerializer(field.FieldType);
				
				expressions.Add(
					Expression.Call(
						writer,
						textWriterWriteString,
						Expression.Constant(
							String.Concat(isFirst ? "" : ",", "\"", field.Name, "\":")
						)
					)
				);
				
				expressions.Add(Expression.Invoke(serializer, Expression.Field(value, field), writer));
				
				isFirst = false;
			}
			expressions.Add(Expression.Call(writer, textWriterWriteChar, Expression.Constant('}')));
			
			Expression body;
			if (!type.IsValueType) {
				body = Expression.IfThenElse(
					Expression.Equal(value, Expression.Constant(null, type)),
					Expression.Call(jsonWriteNull, writer),
					Expression.Block(expressions)
				);
			} else {
				body = Expression.Block(expressions);
			}
			
			return Expression.Lambda(
				Expression.GetActionType(type, typeof(TextWriter)),
				body,
				value,
				writer
			);
		}
		
		public Expression BuildDeserializer(ITypeConvertersCollection generator, Type type) {
			// FIXME: StackOverflow with cyclic references
			
			// Utility methods to process JSON
			var skipSpaces = Finder.GetMethod(() => JSONTools.SkipSpaces(default(TextReader)));
			var readString = Finder.GetMethod(() => JSONTools.ReadString(default(TextReader), out Finder.Type<string>.V));
			var skipObjectStart = Finder.GetMethod(() => JSONTools.SkipObjectStart(default(TextReader)));
			var skipObjectSeparator = Finder.GetMethod(() => JSONTools.SkipFieldSeparator(default(TextReader)));
			var skipObjectFieldSeparator = Finder.GetMethod(() => JSONTools.SkipObjectFieldSeparator(default(TextReader)));
			var skipObjectEnd = Finder.GetMethod(() => JSONTools.SkipObjectEnd(default(TextReader)));
			var isObjectEnd = Finder.GetMethod(() => JSONTools.IsObjectEnd(default(TextReader)));
			var readNull = Finder.GetMethod(() => JSONTools.ReadNull(default(TextReader), out Finder.Type<bool>.V));
			var fail = Finder.GetMethod(() => JSONTools.Fail(default(string)));
			
			// Parameters and method body
			var reader = Expression.Parameter(typeof(TextReader));
			var expressions = new List<Expression>();
			
			// Read initial {
			expressions.Add(
				Expression.IfThen(
					Expression.IsFalse(
						Expression.Call(skipObjectStart, reader)
					),
					Expression.Call(fail, Expression.Constant("Object begin token expected."), Expression.Constant(new object[0]))
				)
			);
			
			// Construct an empty object of type 'type' to store deserialized value
			var value = Expression.Parameter(type);
			expressions.Add(Expression.Assign(value, Expression.New(type)));
			
			// Build a switch with every possible field name as case
			var cases = new List<SwitchCase>();
			var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => !field.HasAttribute<NonSerializedAttribute>());
			foreach (var field in fields) {
				Expression deserializer;
				if (!generator.TryGetDeserializerExpression(field.FieldType, out deserializer))
					throw Errors.NoDeserializer(field.FieldType);
				
				cases.Add(
					Expression.SwitchCase(
						Expression.Block(
							typeof(void),
							Expression.Assign(Expression.Field(value, field), Expression.Invoke(deserializer, reader))
						),
						Expression.Constant(field.Name)
					)
				);
			}
			
			var parsingFieldName = Expression.Parameter(typeof(string));
			
			var breakLabel = Expression.Label();
			
			var unexpectedField = Expression.Call(fail, Expression.Constant("Unexpected field name: \"{0}\"."), Expression.NewArrayInit(typeof(string), parsingFieldName));
			
			expressions.Add(Expression.Call(skipSpaces, reader));
			
			expressions.Add(
				// If there is an object end here it means there is no field in this object, do not even loop
				Expression.IfThen(
					Expression.IsFalse(Expression.Call(isObjectEnd, reader)),
					Expression.Loop(
						Expression.Block(
							new [] { parsingFieldName },
							
							// Read the name of a field from JSON
							Expression.IfThen(
								Expression.IsFalse(
									Expression.Call(readString, reader, parsingFieldName)
								),
								Expression.Call(fail, Expression.Constant("Field name expected."), Expression.Constant(new object[0]))
							),
							
							// Read the key/value separator :
							Expression.Call(skipSpaces, reader),
							Expression.IfThen(
								Expression.IsFalse(
									Expression.Call(skipObjectFieldSeparator, reader)
								),
								Expression.Call(fail, Expression.Constant("Key/Value separator token expected."), Expression.Constant(new object[0]))
							),
							
							// Parse the field (through the previously built switch)
							Expression.Call(skipSpaces, reader),
							cases.Count == 0 ? (Expression)unexpectedField : (Expression)Expression.Switch(
								parsingFieldName,
								unexpectedField,
								cases.ToArray()
							),
							
							// Read the fields separator , if any
							Expression.Call(skipSpaces, reader),
							Expression.IfThen(
								Expression.IsFalse(
									Expression.Call(skipObjectSeparator, reader)
								),
								Expression.Break(breakLabel)
							),
							
							Expression.Call(skipSpaces, reader)
						),
						breakLabel
					)
				)
			);
			
			// Read final }
			//expressions.Add(Expression.Call(skipSpaces, reader));
			expressions.Add(
				Expression.IfThen(
					Expression.IsFalse(
						Expression.Call(skipObjectEnd, reader)
					),
					Expression.Call(fail, Expression.Constant("Object end token expected."), Expression.Constant(new object[0]))
				)
			);
			
			expressions.Add(value);
			
			// If this value is nullable we check if it's null and write 'null' to writer
			Expression body;
			if (!type.IsValueType) {
				var isValueNull = Expression.Parameter(typeof(bool));
				
				var innerExpressions = new List<Expression>();
				innerExpressions.Add(Expression.Call(skipSpaces, reader));
				
				innerExpressions.Add(
					Expression.IfThen(
						Expression.IsFalse(Expression.Call(readNull, reader, isValueNull)),
						Expression.Call(fail, Expression.Constant("Invalid token."), Expression.Constant(new object[0]))
					)
				);
				innerExpressions.Add(
					Expression.Condition(
						isValueNull,
						Expression.Constant(null, type),
						Expression.Block(expressions)
					)
				);
				
				body = Expression.Block(new [] { value, isValueNull }, innerExpressions);
			} else {
				expressions.Insert(0, Expression.Call(skipSpaces, reader));
				body = Expression.Block(new [] { value }, expressions);
			}
			
			//body = Expression.Block(Expression.Call(Finder.GetMethod(() => System.Diagnostics.Debug.WriteLine(default(string))), Expression.Constant("Deserializing " + type.FullName)), body);
			
			return Expression.Lambda(
				Expression.GetDelegateType(typeof(TextReader), type),
				body,
				reader
			);
		}
	}
}
