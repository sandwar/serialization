﻿using System;
using System.IO;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	/// <summary>Provides functions to serialize and deserialize <see cref="Int32" />s.</summary>
	public class IntConverter : SimpleConverter<int>
	{
		public override void Serialize(int value, TextWriter writer) {
			JSONTools.WriteNumber(writer, value);
		}
		
		public override int Deserialize(TextReader reader) {
			double value;
			if (!JSONTools.ReadNumber(reader, out value))
				throw new InvalidDataException();  // failed to parse
			
			return checked((int)value);
		}
	}
}
