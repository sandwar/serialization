﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using SandWar.Tools.Reflection;
using SandWar.Serialization.JSON.Interfaces;
using SandWar.Serialization.JSON.Internal;

namespace SandWar.Serialization.JSON.Converters
{
	public class ArrayDeserializer : ITypeDeserializer
	{
		readonly Type[] types = { typeof(Array) };
		public IEnumerable<Type> SupportedTypes {
			get { return types; }
		}
		
		public Expression BuildDeserializer(ITypeConvertersCollection generator, Type type) {
			//throw new NotImplementedException();
			
			var innerType = type.GetInterface(typeof(IEnumerable<>)).GetGenericArguments()[0];
			
			Expression innerDeserializer;
			if (!generator.TryGetDeserializerExpression(innerType, out innerDeserializer))
				throw Errors.NoDeserializer(innerType);
			
			// Utility methods to process JSON
			var skipSpaces = Finder.GetMethod(() => JSONTools.SkipSpaces(default(TextReader)));
			var readString = Finder.GetMethod(() => JSONTools.ReadString(default(TextReader), out Finder.Type<string>.V));
			var readNumber = Finder.GetMethod(() => JSONTools.ReadNumber(default(TextReader), out Finder.Type<double>.V));
			var readBoolean = Finder.GetMethod(() => JSONTools.ReadBoolean(default(TextReader), out Finder.Type<bool>.V));
			var skipArrayStart = Finder.GetMethod(() => JSONTools.SkipArrayStart(default(TextReader)));
			var skipArraySeparator = Finder.GetMethod(() => JSONTools.SkipFieldSeparator(default(TextReader)));
			var skipArrayEnd = Finder.GetMethod(() => JSONTools.SkipArrayEnd(default(TextReader)));
			var isArrayEnd = Finder.GetMethod(() => JSONTools.IsArrayEnd(default(TextReader)));
			var readNull = Finder.GetMethod(() => JSONTools.ReadNull(default(TextReader), out Finder.Type<bool>.V));
			var fail = Finder.GetMethod(() => JSONTools.Fail(default(string)));
			
			// Parameters and method body
			var reader = Expression.Parameter(typeof(TextReader));
			var expressions = new List<Expression>();
			
			// Read initial [
			expressions.Add(
				Expression.IfThen(
					Expression.IsFalse(
						Expression.Call(skipArrayStart, reader)
					),
					Expression.Call(fail, Expression.Constant("Object begin token expected."), Expression.Constant(new object[0]))
				)
			);
			
			// Construct an empty object of type 'type' to store deserialized value
			var contents = Expression.Parameter(typeof(List<>).MakeGenericType(innerType));
			expressions.Add(Expression.Assign(contents, Expression.New(contents.Type)));
			
			// Array-related methods
			var addElement = Finder.GetMethod<List<object>>(list => list.Add(default(object))).GetMethodDefinitionForType(contents.Type);
			var toArray = Finder.GetMethod<List<object>>(list => list.ToArray()).GetMethodDefinitionForType(contents.Type);
			
			var breakLabel = Expression.Label();
			
			expressions.Add(Expression.Call(skipSpaces, reader));
			
			expressions.Add(
				// If there is an object end here it means there is no field in this object, do not even loop
				Expression.IfThen(
					Expression.IsFalse(Expression.Call(isArrayEnd, reader)),
					Expression.Loop(
						Expression.Block(
							// Parse the value
							Expression.Call(skipSpaces, reader),
							Expression.Call(contents, addElement, Expression.Invoke(innerDeserializer, reader)),
							
							// Read the fields separator , if any
							Expression.Call(skipSpaces, reader),
							Expression.IfThen(
								Expression.IsFalse(
									Expression.Call(skipArraySeparator, reader)
								),
								Expression.Break(breakLabel)
							),
							
							Expression.Call(skipSpaces, reader)
						),
						breakLabel
					)
				)
			);
			
			// Read final ]
			//expressions.Add(Expression.Call(skipSpaces, reader));
			expressions.Add(
				Expression.IfThen(
					Expression.IsFalse(
						Expression.Call(skipArrayEnd, reader)
					),
					Expression.Call(fail, Expression.Constant("Object end token expected."), Expression.Constant(new object[0]))
				)
			);
			
			expressions.Add(Expression.Call(contents, toArray));
			
			// If this value is nullable we check if it's null and write 'null' to writer
			Expression body;
			if (!type.IsValueType) {
				var isValueNull = Expression.Parameter(typeof(bool));
				
				var innerExpressions = new List<Expression>();
				innerExpressions.Add(Expression.Call(skipSpaces, reader));
				
				innerExpressions.Add(
					Expression.IfThen(
						Expression.IsFalse(Expression.Call(readNull, reader, isValueNull)),
						Expression.Call(fail, Expression.Constant("Invalid token."), Expression.Constant(new object[0]))
					)
				);
				innerExpressions.Add(
					Expression.Condition(
						isValueNull,
						Expression.Constant(null, type),
						Expression.Block(expressions)
					)
				);
				
				body = Expression.Block(new [] { contents, isValueNull }, innerExpressions);
			} else {
				expressions.Insert(0, Expression.Call(skipSpaces, reader));
				body = Expression.Block(new [] { contents }, expressions);
			}
			
			//body = Expression.Block(Expression.Call(Finder.GetMethod(() => System.Diagnostics.Debug.WriteLine(default(string))), Expression.Constant("Deserializing " + type.FullName)), body);
			
			return Expression.Lambda(
				Expression.GetDelegateType(typeof(TextReader), type),
				body,
				reader
			);
		}
	}
}
