﻿using System;
using System.IO;
using System.Text;
using fastJSON;

namespace SandWar.Serialization
{
	/// <summary>Allows (de)serialization to/from JSON format.</summary>
	public class JSONSerializer : IStringSerializer, IStreamSerializer
	{
		readonly bool pretty;
		readonly Encoding encoding;
		readonly JSONParameters config;
		
		/// <summary>Initialize a new JSON serializer.</summary>
		/// <param name="pretty">Specifies if the output code should be prettyfied for human reading.</param>
		/// <param name="encoding"></param>
		public JSONSerializer(bool pretty, Encoding encoding) {
			this.pretty = pretty;
			this.encoding = encoding;
			
			config = new JSONParameters() {
				SerializeNullValues = false,
				DateTimeMilliseconds = true,
				UseUTCDateTime = true,
				UseExtensions = false,
				SerializerMaxDepth = 7
			};
		}
		
		/// <summary>Initialize a new JSON serializer.</summary>
		/// <param name="pretty">Specifies if the output code should be prettyfied for human reading.</param>
		public JSONSerializer(bool pretty) : this(pretty, Encoding.Default) { }
		
		/// <summary>Initialize a new JSON serializer.</summary>
		public JSONSerializer() : this(false) { }
		
		/// <summary>Serialize an object.</summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="stream">The stream to write serialized object to.</param>
		public void Serialize<T>(T obj, Stream stream) {
			var sw = new StreamWriter(stream, encoding);
			sw.Write(serialize(obj));
			sw.Flush();
		}
		
		/// <summary>Deserialize an object.</summary>
		/// <param name="stream">The stream to read the object from.</param>
		/// <param name="obj">The object to write the deserialized data to, can be null.</param>
		/// <returns>The deserialized object.</returns>
		public T Deserialize<T>(Stream stream, T obj) {
			var sr = new StreamReader(stream, encoding);
			return deserialize(sr.ReadToEnd(), obj);
		}
		
		/// <summary>Serialize an object.</summary>
		/// <param name="obj">The object to serialize.</param>
		/// <returns>A JSON string representing <paramref name="obj"/>.</returns>
		public string Serialize<T>(T obj) {
			return serialize(obj);
		}
		
		/// <summary>Deserialize an object.</summary>
		/// <param name="json">The JSON string that contains the serialized object.</param>
		/// <param name="obj">The object to write the deserialized data to, can be null.</param>
		/// <returns>The deserialized object.</returns>
		public T Deserialize<T>(string json, T obj) {
			return deserialize(json, obj);
		}
		
		string serialize<T>(T obj) {
			return pretty ? JSON.ToJSON(obj, config) : JSON.ToNiceJSON(obj, config);
		}
		
		T deserialize<T>(string json, T obj) {
			return ReferenceEquals(obj, null) ? (T)JSON.FillObject(obj, json) : JSON.ToObject<T>(json);
		}
	}
}
