﻿using System;

namespace SandWar.Serialization
{
	/// <summary>Allows serializing objects to <see cref="String"/>s and getting them back.</summary>
	public interface IStringSerializer
	{
		/// <summary>Serialize an object.</summary>
		/// <param name="obj">The object to serialize.</param>
		/// <returns>A string representing <paramref name="obj"/>.</returns>
		string Serialize<T>(T obj);
		
		/// <summary>Deserialize an object.</summary>
		/// <param name="str">The string that contains the serialized object.</param>
		/// <param name="obj">The object to write the deserialized data to, can be null.</param>
		/// <returns>The deserialized object.</returns>
		T Deserialize<T>(string str, T obj);
	}
}
