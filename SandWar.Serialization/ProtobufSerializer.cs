﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ProtoBuf;
using ProtoBuf.Meta;

namespace SandWar.Serialization
{
	/// <summary>Allows very fast serialization with minimum overhead.</summary>
	public class ProtobufSerializer : IStreamSerializer
	{
		// FIXME: Serialize((Fruit)apple) then Serialize((Apple)apple) produces unexpected results
		// 1. serialize a specialized type cast as generic => model bounds generic fields to specialized type
		// 2. serialize the specialized type without casting => model ignores fields in the specialized type
		// problems also occur when deserializing, and if the order of operations is reversed
		
		readonly RuntimeTypeModel model;
		readonly HashSet<Type> toRegister;
		
		/// <summary>Instantiate a new Protobuf serializer.</summary>
		public ProtobufSerializer() {
			model = TypeModel.Create();
			model.AllowParseableTypes = false;
			model.AutoAddMissingTypes = false;
			model.AutoCompile = true;
			
			toRegister = new HashSet<Type>();
		}
		
		/// <summary>Serialize an object.</summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="stream">The stream to write serialized object to.</param>
		public void Serialize<T>(T obj, Stream stream) {
			//if(!model.CanSerialize(typeof(T)))
			//	register(typeof(T));
			//
			//model.SerializeWithLengthPrefix(stream, obj, typeof(T), PrefixStyle.Fixed32, 0);
			
			serialize<T>(obj, stream, true);
		}
		
		/// <summary>Deserialize an object.</summary>
		/// <param name="stream">The stream to read the object from.</param>
		/// <param name="obj">The object to write the deserialized data to, can be null.</param>
		/// <returns>The deserialized object.</returns>
		public T Deserialize<T>(Stream stream, T obj) {
			//if(!model.CanSerialize(typeof(T)))
			//	register(typeof(T));
			//
			//return (T)model.DeserializeWithLengthPrefix(stream, obj, typeof(T), PrefixStyle.Fixed32, 0);
			
			return deserialize<T>(stream, obj, true);
		}
		
		void serialize<T>(T obj, Stream stream, bool tryAgain) {
			Type behaveAs = typeof(T);
			Type objectType = ReferenceEquals(obj, null) ? behaveAs : obj.GetType();
			
			long streamPos = 0;
			if(!stream.CanSeek && !model.CanSerialize(objectType))
				register(objectType, behaveAs);
			else
				streamPos = stream.Position;
			
			try {
				model.SerializeWithLengthPrefix(stream, obj, objectType, PrefixStyle.Fixed32, 0);
			} catch(InvalidOperationException) {
				if(!tryAgain || !stream.CanSeek)
					throw;
				
				register(objectType, behaveAs);
				
				stream.Position = streamPos;
				
				// try again after registering T
				serialize<T>(obj, stream, false);
			}
		}
		
		T deserialize<T>(Stream stream, T obj, bool tryAgain) {
			Type behaveAs = typeof(T);
			Type objectType = ReferenceEquals(obj, null) ? behaveAs : obj.GetType();
			
			long streamPos = 0;
			if(!stream.CanSeek && !model.CanSerialize(objectType))
				register(objectType, behaveAs);
			else
				streamPos = stream.Position;
			
			try {
				return (T)model.DeserializeWithLengthPrefix(stream, obj, objectType, PrefixStyle.Fixed32, 0);
			} catch(InvalidOperationException) {
				if(!tryAgain || !stream.CanSeek)
					throw;
				
				register(objectType, behaveAs);
				
				stream.Position = streamPos;
				
				// try again after registering T
				return deserialize(stream, obj, false);
			}
		}
		
		void register(Type t, Type ast) {
			lock(model) {
				registerSub(t, ast);
				
				// all sub-types registered
				toRegister.Clear();
			}
		}
		
		void registerSub(Type t, Type ast) {
			// proceed only if not already registered
			if(model.CanSerialize(t))
				return;
			
			int i = 0;
			var meta = model.Add(t, false);
			foreach(var field in ast.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)) {
				if(field.GetCustomAttributes(typeof(NonSerializedAttribute), true).Length == 0)
					meta.AddField(++i, field.Name);
				
				// save sub-types, will check later if already registered
				toRegister.Add(field.FieldType);
			}
			
			foreach(var generic in ast.GetGenericArguments()) {
				// save sub-types, will check later if already registered
				toRegister.Add(generic);
			}
			
			// register sub-types
			foreach(var type in toRegister.ToArray())
				registerSub(type, type);
		}
	}
}
