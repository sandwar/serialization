﻿using System;
using System.IO;

namespace SandWar.Serialization
{
	/// <summary>Allows serializing objects to <see cref="Stream"/>s, and getting them back.</summary>
	public interface IStreamSerializer
	{
		/// <summary>Serialize an object.</summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="stream">The stream to write serialized object to.</param>
		void Serialize<T>(T obj, Stream stream);
		
		/// <summary>Deserialize an object.</summary>
		/// <param name="stream">The stream to read the object from.</param>
		/// <param name="obj">The object to write the deserialized data to, can be null.</param>
		/// <returns>The deserialized object.</returns>
		T Deserialize<T>(Stream stream, T obj);
	}
}
