﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace SandWar.Serialization.TestUnit
{
	[TestFixture]
	public class JSONSerializerTests
	{
		public class MyClass {
			public int Int;
			public long Long;
			public string String;
			public int? NullableInt;
			public byte[] Bytes;
			public List<short> Shorts;
		}
		
		public class MyComposedClass {
			public MyClass Object;
		}
		
		public class MyContainer<T> {
			public T Val;
		}
		
		public class MyPrivateContainer {
			// disable once FieldCanBeMadeReadOnly.Local
			int inVal;
			
			public MyPrivateContainer(int val) {
				this.inVal = val;
			}
			
			public int GetVal() {
				return inVal;
			}
		}
		
		public abstract class MyGenericClass {
			public string Generic;
		}
		
		public class MySpecializedClass : MyGenericClass {
			public string Specialized;
		}
		
		public class MyPartiallyIgnoredClass {
			[NonSerialized]
			public string Ignored;
			public string Serialized;
		}
		
		[Test]
		public void BasicSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyClass() {
				Int = 123,
				Long = 456,
				String = "test",
				NullableInt = 789,
				Bytes = new byte[] { 12, 34, 56 },
				Shorts = new List<short>() { 147, 258, 369 }
			};
			
			var myOutputObject = new MyClass();
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual(123, myOutputObject.Int, "#1");
			Assert.AreEqual(456, myOutputObject.Long, "#2");
			Assert.AreEqual("test", myOutputObject.String, "#3");
			Assert.AreEqual(789, myOutputObject.NullableInt, "#4");
			
			Assert.AreEqual(new byte[] { 12, 34, 56 }, myOutputObject.Bytes, "#5");
			
			Assert.AreEqual(new List<short>() { 147, 258, 369 }, myOutputObject.Shorts, "#6");
		}
		
		[Test]
		public void ComposedSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyComposedClass() {
				Object = new MyClass() {
					Int = 321
				}
			};
			
			var myOutputObject = new MyComposedClass();
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.IsNotNull(myOutputObject.Object, "#1");
			Assert.AreEqual(321, myOutputObject.Object.Int, "#2");
		}
		
		[Test]
		public void GenericSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyContainer<int>() {
				Val = 654
			};
			
			var myOutputObject = new MyContainer<int>();
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual(654, myOutputObject.Val, "#1");
		}
		
		[Test]
		public void DateTimeSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyContainer<DateTime>() {
				Val = new DateTime(1994, 7, 17, 1, 2, 3, DateTimeKind.Utc)
			};
			
			var myOutputObject = new MyContainer<DateTime>();
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual(new DateTime(1994, 7, 17, 1, 2, 3, DateTimeKind.Utc), myOutputObject.Val, "#1");
		}
		
		[Test]
		public void PrivateSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyPrivateContainer(789);
			var myOutputObject = new MyPrivateContainer(0);
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual(789, myOutputObject.GetVal(), "#1");
		}
		
		[Test]
		public void InheritanceTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MySpecializedClass();
			var myOutputObject = new MySpecializedClass();
			
			myInputObject.Generic = "genericValue";
			myInputObject.Specialized = "specializedValue";
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual("genericValue", myOutputObject.Generic, "#1");
			Assert.AreEqual("specializedValue", myOutputObject.Specialized, "#2");
		}
		
		[Test]
		public void InheritanceIgnoreSpecializedTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MySpecializedClass();
			var myOutputObject = new MySpecializedClass();
			
			myInputObject.Generic = "genericValue";
			myInputObject.Specialized = "shoudIgnoreThisValue";
			myOutputObject = (MySpecializedClass)serializeAndDeserialize<MyGenericClass>(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual("genericValue", myOutputObject.Generic, "#1");
			Assert.AreNotEqual("shoudIgnoreThisValue", myOutputObject.Specialized, "#2");
		}
		
		[Test]
		public void NonSerializedTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new MyPartiallyIgnoredClass();
			var myOutputObject = new MyPartiallyIgnoredClass();
			
			myInputObject.Serialized = "serializedValue";
			myInputObject.Ignored = "ignoredValue";
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual("serializedValue", myOutputObject.Serialized, "#1");
			Assert.AreNotEqual("originalValue", myOutputObject.Ignored, "#2");
		}
		
		[Test]
		public void CollectionsSerializationTest() {
			var serializer = new JSONSerializer();
			
			var myInputObject = new List<MyClass>() {
				new MyClass {
					Int = 123
				}
			};
			
			var myOutputObject = new List<MyClass>();
			myOutputObject = serializeAndDeserialize(serializer, myInputObject, myOutputObject);
			
			Assert.AreEqual(1, myOutputObject.Count, "#1");
			Assert.AreEqual(123, myOutputObject[0].Int, "#2");
		}
		
		T serializeAndDeserialize<T>(IStreamSerializer serializer, T input, T output) {
			using(var ms = new MemoryStream()) {
				serializer.Serialize<T>(input, ms);
				
				ms.Flush();
				ms.Position = 0;
				
				var jsonDebug = new StreamReader(ms).ReadToEnd();
				
				ms.Position = 0;
				
				return serializer.Deserialize<T>(ms, output);
			}
		}
	}
}
